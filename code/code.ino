#include <stdint.h>

int RELAY_TURN = 6;
int RELAY_PLUS = 5;
int RELAY_MINUS = 4;
int START_BUTTON = 11;
int DOOR_SENSOR = 12;
int BEEP = 13;

uint32_t closedDoorTime = 0;
uint32_t openedDoorTime = 0;
bool isDoorClose = false;

uint32_t startButtonPressed = 0;

uint16_t secretDelay = 100;
uint16_t phaseDelay = 10000;
uint16_t relayDelay = 500;
uint16_t phaseLenght1 = 20000;
uint32_t phaseLenght2 = 180000;

bool processPhases = false;
bool finishedPhase1 = false;
bool finishedPhase2 = false;
bool finishedPhase3 = false;

uint32_t phase1 = 0;
uint32_t phase2 = 0;
uint32_t phase3 = 0;

void setup() {
  Serial.begin(9600);

  pinMode(RELAY_TURN, OUTPUT);
  pinMode(RELAY_PLUS, OUTPUT);
  pinMode(RELAY_MINUS, OUTPUT);

  pinMode(START_BUTTON, INPUT);
  pinMode(DOOR_SENSOR, INPUT);
  pinMode(BEEP, OUTPUT);
}

void loop() {
  if (digitalRead(DOOR_SENSOR) == HIGH && closedDoorTime == 0) {
    closedDoorTime = millis();
  } else if (digitalRead(DOOR_SENSOR) == HIGH && millis() > closedDoorTime + secretDelay) {
    isDoorClose = true;
    openedDoorTime = 0;
  }

  if (digitalRead(DOOR_SENSOR) == LOW && openedDoorTime == 0) {
    openedDoorTime = millis();
  } else if (digitalRead(DOOR_SENSOR) == LOW && millis() > openedDoorTime + secretDelay) {
    isDoorClose = false;
    closedDoorTime = 0;
    stop();
    openedDoorBeep();
  }

  if (digitalRead(START_BUTTON) == HIGH && isDoorClose && startButtonPressed == 0) {
    startButtonPressed = millis();
  } else if (digitalRead(START_BUTTON) == HIGH && isDoorClose && millis() > startButtonPressed + secretDelay) {
    startButtonPressed = 0;
    start();
  }

  processProgram();
}

void start() {
  Serial.println("start");
  processPhases = true;
}

void processProgram() {
  if (!processPhases) {
    return;
  }

  if (phase1 == 0) {
    phase1 = millis();
    oddPhase();
  }

  if (!finishedPhase1 && phase1 != 0 && millis() > phase1 + phaseLenght1) {
    finishedPhase1 = true;
    stopPhase();
    evenPhase();
    phase2 = millis();
  }

  
  if (!finishedPhase2 && phase2 != 0 && millis() > phase2 + phaseLenght2) {
    finishedPhase2 = true;
    stopPhase();
    oddPhase();
    phase3 = millis();
  }

  if (!finishedPhase3 && phase3 != 0 && millis() > phase3 + phaseLenght2) {
    finishedPhase3 = true;
    stop();
    finished();
  }
}

void oddPhase() {
  Serial.println("oddPhase");
  
  delay(relayDelay);
  digitalWrite(RELAY_TURN, LOW);

  digitalWrite(RELAY_PLUS, LOW);
  digitalWrite(RELAY_MINUS, LOW);
  delay(relayDelay);
  digitalWrite(RELAY_TURN, HIGH);
}

void evenPhase() {
  Serial.println("evenPhase");
  
  delay(relayDelay);
  digitalWrite(RELAY_TURN, LOW);

  digitalWrite(RELAY_PLUS, HIGH);
  digitalWrite(RELAY_MINUS, HIGH);

  delay(relayDelay);
  digitalWrite(RELAY_TURN, HIGH);
}

void stopPhase() {
  Serial.println("stopPhase");
  digitalWrite(RELAY_TURN, LOW);  
  delay(phaseDelay);
}

void stop() {
  Serial.println("stop");
  processPhases = false;

  finishedPhase1 = false;
  finishedPhase2 = false;
  finishedPhase3 = false;

  phase1 = 0;
  phase2 = 0;
  phase3 = 0;

  digitalWrite(RELAY_TURN, LOW);
  digitalWrite(RELAY_PLUS, LOW);
  digitalWrite(RELAY_MINUS, LOW);
}

void openedDoorBeep() {
  Serial.println("openedDoorBeep");
  beep(50);
  delay(2000);
}

void finished() {
  Serial.println("finished");
  beep(500);
  delay(500);
  beep(500);
}

void beep(uint32_t time) {
  digitalWrite(BEEP, HIGH);
  delay(time);
  digitalWrite(BEEP, LOW);
}
